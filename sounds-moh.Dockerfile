ARG BASE_IMAGE=quay.io/centos/centos:stream9
FROM $BASE_IMAGE

WORKDIR /root/
RUN yum install -y rpm-build rpmdevtools yum-utils epel-release wget
ADD asterisk-sounds-*.spec /root/rpmbuild/SPECS/
RUN mkdir -p /root/rpmbuild/SOURCES
RUN spectool -g -R /root/rpmbuild/SPECS/asterisk-sounds-moh-opsound.spec
RUN yum-builddep -y /root/rpmbuild/SPECS/asterisk-sounds-moh-opsound.spec
RUN rpmbuild -ba /root/rpmbuild/SPECS/asterisk-sounds-moh-opsound.spec

