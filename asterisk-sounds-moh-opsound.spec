%global sounds_dir %{_sharedstatedir}/asterisk/moh

Name:           asterisk-sounds-moh-opsound
Version:        2.03
Release:        1%{?dist}
Summary:        Music On-Hold sounds for Asterisk
License:        CC-BY-SA
URL:            http://www.asterisk.org/

Source0:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-moh-opsound-alaw-%{version}.tar.gz
Source1:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-moh-opsound-g722-%{version}.tar.gz
Source2:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-moh-opsound-g729-%{version}.tar.gz
Source3:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-moh-opsound-gsm-%{version}.tar.gz
Source4:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-moh-opsound-siren7-%{version}.tar.gz
Source5:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-moh-opsound-siren14-%{version}.tar.gz
Source6:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-moh-opsound-sln16-%{version}.tar.gz
Source7:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-moh-opsound-ulaw-%{version}.tar.gz
Source8:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-moh-opsound-wav-%{version}.tar.gz

BuildArch:      noarch

%description
Music On-Hold sound files for Asterisk.

%package alaw
Summary: Music On-Hold ALAW sound files for Asterisk
Requires: asterisk >= 1.4.0
Provides: asterisk-sounds-moh-opsound = %{version}-%{release}

%description alaw
Music On-Hold ALAW sound files for Asterisk.

%package g722
Summary: Music On-Hold G.722 sound files for Asterisk
Requires: asterisk >= 1.4.0
Provides: asterisk-sounds-moh-opsound = %{version}-%{release}

%description g722
Music On-Hold G.722 sound files for Asterisk.

%package g729
Summary: Music On-Hold G.729 sound files for Asterisk
Requires: asterisk >= 1.4.0
Provides: asterisk-sounds-moh-opsound = %{version}-%{release}

%description g729
Music On-Hold G.729 sound files for Asterisk.

%package gsm
Summary: Music On-Hold GSM sound files for Asterisk
Requires: asterisk >= 1.4.0
Provides: asterisk-sounds-moh-opsound = %{version}-%{release}

%description gsm
Music On-Hold GSM sound files for Asterisk.

%package siren7
Summary: Music On-Hold Siren7 sound files for Asterisk
Requires: asterisk >= 1.4.0
Provides: asterisk-sounds-moh-opsound = %{version}-%{release}

%description siren7
Music On-Hold Siren7 sound files for Asterisk.

%package siren14
Summary: Music On-Hold GSM sound files for Asterisk
Requires: asterisk >= 1.4.0
Provides: asterisk-sounds-moh-opsound = %{version}-%{release}

%description siren14
Music On-Hold Siren14 sound files for Asterisk.

%package sln16
Summary: Music On-Hold SLN16 sound files for Asterisk
Requires: asterisk >= 1.4.0
Provides: asterisk-sounds-moh-opsound = %{version}-%{release}

%description sln16
Music On-Hold SLN16 sound files for Asterisk.

%package ulaw
Summary: Music On-Hold ULAW sound files for Asterisk
Requires: asterisk >= 1.4.0
Provides: asterisk-sounds-moh-opsound = %{version}-%{release}

%description ulaw
Music On-Hold ULAW sound files for Asterisk.

%package wav
Summary: Music On-Hold WAV sound files for Asterisk
Requires: asterisk >= 1.4.0
Provides: asterisk-sounds-moh-opsound = %{version}-%{release}

%description wav
Music On-Hold WAV sound files for Asterisk.

%prep

%setup -c -T 

%build

for file in %{S:0} %{S:1} %{S:2} %{S:3} %{S:4} %{S:5} %{S:6} %{S:7} %{S:8}
do
  tar --list --file $file | grep -E '.(alaw|g722|g729|gsm|siren7|siren14|sln16|ulaw|wav)$' | sed -e 's!^!%{sounds_dir}/!' > `basename $file .tar.gz`.list
  tar --extract --directory . --file $file
done

%install
rm -rf %{buildroot}

for file in `cat *.list | sed -e 's!^%{sounds_dir}/!!'`
do
  install -D -m644 $file %{buildroot}%{sounds_dir}/$file
done

%files

%files alaw -f asterisk-moh-opsound-alaw-%{version}.list
%doc asterisk-moh-opsound-alaw-%{version}.list
%license LICENSE-asterisk-moh-opsound-alaw
%doc CHANGES-asterisk-moh-opsound-alaw
%doc CREDITS-asterisk-moh-opsound-alaw

%files g722 -f asterisk-moh-opsound-g722-%{version}.list
%doc asterisk-moh-opsound-g722-%{version}.list
%license LICENSE-asterisk-moh-opsound-g722
%doc CHANGES-asterisk-moh-opsound-g722
%doc CREDITS-asterisk-moh-opsound-g722

%files g729 -f asterisk-moh-opsound-g729-%{version}.list
%doc asterisk-moh-opsound-g729-%{version}.list
%license LICENSE-asterisk-moh-opsound-g729
%doc CHANGES-asterisk-moh-opsound-g729
%doc CREDITS-asterisk-moh-opsound-g729

%files gsm -f asterisk-moh-opsound-gsm-%{version}.list
%doc asterisk-moh-opsound-gsm-%{version}.list
%license LICENSE-asterisk-moh-opsound-gsm
%doc CHANGES-asterisk-moh-opsound-gsm
%doc CREDITS-asterisk-moh-opsound-gsm

%files siren7 -f asterisk-moh-opsound-siren7-%{version}.list
%doc asterisk-moh-opsound-siren7-%{version}.list
%license LICENSE-asterisk-moh-opsound-siren7
%doc CHANGES-asterisk-moh-opsound-siren7
%doc CREDITS-asterisk-moh-opsound-siren7

%files siren14 -f asterisk-moh-opsound-siren14-%{version}.list
%doc asterisk-moh-opsound-siren14-%{version}.list
%license LICENSE-asterisk-moh-opsound-siren14
%doc CHANGES-asterisk-moh-opsound-siren14
%doc CREDITS-asterisk-moh-opsound-siren14

%files sln16 -f asterisk-moh-opsound-sln16-%{version}.list
%doc asterisk-moh-opsound-sln16-%{version}.list
%license LICENSE-asterisk-moh-opsound-sln16
%doc CHANGES-asterisk-moh-opsound-sln16
%doc CREDITS-asterisk-moh-opsound-sln16

%files ulaw -f asterisk-moh-opsound-ulaw-%{version}.list
%doc asterisk-moh-opsound-ulaw-%{version}.list
%license LICENSE-asterisk-moh-opsound-ulaw
%doc CHANGES-asterisk-moh-opsound-ulaw
%doc CREDITS-asterisk-moh-opsound-ulaw

%files wav -f asterisk-moh-opsound-wav-%{version}.list
%doc asterisk-moh-opsound-wav-%{version}.list
%license LICENSE-asterisk-moh-opsound-wav
%doc CHANGES-asterisk-moh-opsound-wav
%doc CREDITS-asterisk-moh-opsound-wav

%changelog
* Sun Jan 12 2020 Cloudonix Ops <ops@cloudonix.io> - 2.03-1
- Initial build for Cloudonix based on Fedora asteris-moh-opsound spec
