IMAGE := asterisk-sounds-build-el

build: sounds

sounds: sounds-core sounds-extra sounds-moh

sounds-core-el7:
	podman build -t $(IMAGE) -f sounds-core.Dockerfile --build-arg=BASE_IMAGE=quay.io/centos/centos:7 .

sounds-core-el8:
	podman build -t $(IMAGE) -f sounds-core.Dockerfile --build-arg=BASE_IMAGE=quay.io/centos/centos:stream8 .

sounds-core-el9:
	podman build -t $(IMAGE) -f sounds-core.Dockerfile --build-arg=BASE_IMAGE=quay.io/centos/centos:stream9 .

sounds-core: sounds-core-el7 sounds-core-el8 sounds-core-el9

sounds-extra-el7:
	podman build -t $(IMAGE) -f sounds-extra.Dockerfile --build-arg=BASE_IMAGE=quay.io/centos/centos:7 .

sounds-extra-el8:
	podman build -t $(IMAGE) -f sounds-extra.Dockerfile --build-arg=BASE_IMAGE=quay.io/centos/centos:stream8 .

sounds-extra-el9:
	podman build -t $(IMAGE) -f sounds-extra.Dockerfile --build-arg=BASE_IMAGE=quay.io/centos/centos:stream9 .

sounds-extra: sounds-extra-el7 sounds-extra-el8 sounds-extra-el9

sounds-moh-el7:
	podman build -t $(IMAGE) -f sounds-moh.Dockerfile --build-arg=BASE_IMAGE=quay.io/centos/centos:7 .

sounds-moh-el8:
	podman build -t $(IMAGE) -f sounds-moh.Dockerfile --build-arg=BASE_IMAGE=quay.io/centos/centos:stream8 .

sounds-moh-el9:
	podman build -t $(IMAGE) -f sounds-moh.Dockerfile --build-arg=BASE_IMAGE=quay.io/centos/centos:stream9 .

sounds-moh: sounds-moh-el7 sounds-moh-el8 sounds-moh-el9
