%global sounds_dir %{_sharedstatedir}/asterisk/sounds

%bcond_without en
%bcond_with en_GB
%bcond_with fr

Name:           asterisk-sounds-extra
Version:        1.5.2
Release:        1%{?dist}
Summary:        Extra sounds for Asterisk
License:        CC-BY-SA
URL:            http://www.asterisk.org/

%if %{with en}
Source0:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en-alaw-%{version}.tar.gz
Source1:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en-g722-%{version}.tar.gz
Source2:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en-g729-%{version}.tar.gz
Source3:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en-gsm-%{version}.tar.gz
Source4:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en-siren7-%{version}.tar.gz
Source5:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en-siren14-%{version}.tar.gz
Source6:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en-sln16-%{version}.tar.gz
Source7:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en-ulaw-%{version}.tar.gz
Source8:        http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en-wav-%{version}.tar.gz
%endif

%if %{with fr}
Source20:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-fr-alaw-%{version}.tar.gz
Source21:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-fr-g722-%{version}.tar.gz
Source22:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-fr-g729-%{version}.tar.gz
Source23:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-fr-gsm-%{version}.tar.gz
Source24:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-fr-siren7-%{version}.tar.gz
Source25:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-fr-siren14-%{version}.tar.gz
Source26:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-fr-sln16-%{version}.tar.gz
Source27:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-fr-ulaw-%{version}.tar.gz
Source28:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-fr-wav-%{version}.tar.gz
%endif

%if %{with en_GB}
Source60:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en_GB-alaw-%{version}.tar.gz
Source61:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en_GB-g722-%{version}.tar.gz
Source62:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en_GB-g729-%{version}.tar.gz
Source63:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en_GB-gsm-%{version}.tar.gz
Source64:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en_GB-siren7-%{version}.tar.gz
Source65:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en_GB-siren14-%{version}.tar.gz
Source66:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en_GB-sln16-%{version}.tar.gz
Source67:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en_GB-ulaw-%{version}.tar.gz
Source68:       http://downloads.asterisk.org/pub/telephony/sounds/releases/asterisk-extra-sounds-en_GB-wav-%{version}.tar.gz
%endif

BuildArch:      noarch

%description
Extra sound files for Asterisk.

%if %{with en}
%package en
Summary: Extra English sound files for Asterisk
Requires: asterisk >= 1.4.0

%description en
Extra English sound files for Asterisk.

%package en-alaw
Summary: Extra English ALAW sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en-alaw
Extra English ALAW sound files for Asterisk.

%package en-g722
Summary: Extra English G.722 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en-g722
Extra English G.722 sound files for Asterisk.

%package en-g729
Summary: Extra English G.729 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en-g729
Extra English G.729 sound files for Asterisk.

%package en-gsm
Summary: Extra English GSM sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en-gsm
Extra English GSM sound files for Asterisk.

%package en-siren7
Summary: Extra English Siren7 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en-siren7
Extra English Siren7 sound files for Asterisk.

%package en-siren14
Summary: Extra English GSM sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en-siren14
Extra English Siren14 sound files for Asterisk.

%package en-sln16
Summary: Extra English SLN16 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en-sln16
Extra English SLN16 sound files for Asterisk.

%package en-ulaw
Summary: Extra English ULAW sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en-ulaw
Extra English ULAW sound files for Asterisk.

%package en-wav
Summary: Extra English WAV sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en-wav
Extra English WAV sound files for Asterisk.
%endif

%if %{with fr}
%package fr
Summary: Extra French sound files for Asterisk
Requires: asterisk >= 1.4.0

%description fr
Extra French sound files for Asterisk.

%package fr-alaw
Summary: Extra French ALAW sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-fr = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description fr-alaw
Extra French ALAW sound files for Asterisk.

%package fr-g722
Summary: Extra French G.722 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-fr = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description fr-g722
Extra French G.722 sound files for Asterisk.

%package fr-g729
Summary: Extra French G.729 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-fr = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description fr-g729
Extra French G.729 sound files for Asterisk.

%package fr-gsm
Summary: Extra French GSM sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-fr = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description fr-gsm
Extra French GSM sound files for Asterisk.

%package fr-siren7
Summary: Extra French Siren7 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-fr = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description fr-siren7
Extra French Siren7 sound files for Asterisk.

%package fr-siren14
Summary: Extra French Siren14 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-fr = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description fr-siren14
Extra French Siren14 sound files for Asterisk.

%package fr-sln16
Summary: Extra French SLN16 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-fr = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description fr-sln16
Extra French SLN16 sound files for Asterisk.

%package fr-ulaw
Summary: Extra French ULAW sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-fr = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description fr-ulaw
Extra French ULAW sound files for Asterisk.

%package fr-wav
Summary: Extra French WAV sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-fr = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description fr-wav
Extra French WAV sound files for Asterisk.
%endif

%if %{with en_GB}
%package en_GB
Summary: Extra English (United Kingdom) sound files for Asterisk
Requires: asterisk >= 1.4.0

%description en_GB
Extra English (United Kingdom) sound files for Asterisk.

%package en_GB-alaw
Summary: Extra English (United Kingdom) ALAW sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en_GB = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en_GB-alaw
Extra English (United Kingdom) ALAW sound files for Asterisk.

%package en_GB-g722
Summary: Extra English (United Kingdom) G.722 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en_GB = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en_GB-g722
Extra English (United Kingdom) G.722 sound files for Asterisk.

%package en_GB-g729
Summary: Extra English (United Kingdom) G.729 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en_GB = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en_GB-g729
Extra English (United Kingdom) G.729 sound files for Asterisk.

%package en_GB-gsm
Summary: Extra English (United Kingdom) GSM sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en_GB = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en_GB-gsm
Extra English (United Kingdom) GSM sound files for Asterisk.

%package en_GB-siren7
Summary: Extra English (United Kingdom) Siren7 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en_GB = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en_GB-siren7
Extra English (United Kingdom) Siren7 sound files for Asterisk.

%package en_GB-siren14
Summary: Extra English (United Kingdom) Siren14 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en_GB = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en_GB-siren14
Extra English (United Kingdom) Siren14 sound files for Asterisk.

%package en_GB-sln16
Summary: Extra English (United Kingdom) SLN16 sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en_GB = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en_GB-sln16
Extra English (United Kingdom) SLN16 sound files for Asterisk.

%package en_GB-ulaw
Summary: Extra English (United Kingdom) ULAW sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en_GB = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en_GB-ulaw
Extra English (United Kingdom) ULAW sound files for Asterisk.

%package en_GB-wav
Summary: Extra English (United Kingdom) WAV sound files for Asterisk
Requires: asterisk >= 1.4.0
Requires: asterisk-sounds-extra-en_GB = %{version}-%{release}
Provides: asterisk-sounds-extra = %{version}-%{release}

%description en_GB-wav
Extra English (United Kingdom) WAV sound files for Asterisk.
%endif

%prep

%setup -c -T 

%build

for file in %{S:0} %{S:1} %{S:2} %{S:3} %{S:4} %{S:5} %{S:6} %{S:7} %{S:8}
do
  tar --list --file $file | grep -E '.(alaw|g722|g729|gsm|siren7|siren14|sln16|ulaw|wav)$' | sed -e 's!^!%{sounds_dir}/!' > `basename $file .tar.gz`.list
  tar --extract --directory . --file $file
done

%if %{with fr}
mkdir fr

for file in %{S:20} %{S:21} %{S:22} %{S:23} %{S:24} %{S:25} %{S:26} %{S:27} %{S:28}
do
  tar --list --file $file | grep -E '.(alaw|g722|g729|gsm|siren7|siren14|sln16|ulaw|wav)$' | sed -e 's!^!%{sounds_dir}/fr/!' > `basename $file .tar.gz`.list
  tar --extract --directory ./fr/  --file $file
done

iconv -f iso-8859-1 -t utf-8 < fr/extra-sounds-fr.txt > fr/extra-sounds-fr.txt.tmp
touch --reference fr/extra-sounds-fr.txt fr/extra-sounds-fr.txt.tmp
mv fr/extra-sounds-fr.txt.tmp fr/extra-sounds-fr.txt
%endif

%if %{with en_GB}
mkdir en_GB

for file in %{S:60} %{S:61} %{S:62} %{S:63} %{S:64} %{S:65} %{S:66} %{S:67} %{S:68}
do
  tar --list --file $file | grep -E '.(alaw|g722|g729|gsm|siren7|siren14|sln16|ulaw|wav)$' | sed -e 's!^!%{sounds_dir}/en_GB/!' > `basename $file .tar.gz`.list
  tar --extract --directory ./en_GB/  --file $file
done
%endif

%install
rm -rf %{buildroot}

for file in `cat *.list | sed -e 's!^%{sounds_dir}/!!'`
do
  install -D -m644 $file %{buildroot}%{sounds_dir}/$file
done

%if %{with en}
%files en
%doc extra-sounds-en.txt
%doc CHANGES-asterisk-extra-en-%{version}
%doc CREDITS-asterisk-extra-en-%{version}
%license LICENSE-asterisk-extra-en-%{version}

%files en-alaw -f asterisk-extra-sounds-en-alaw-%{version}.list
%doc asterisk-extra-sounds-en-alaw-%{version}.list

%files en-g722 -f asterisk-extra-sounds-en-g722-%{version}.list
%doc asterisk-extra-sounds-en-g722-%{version}.list

%files en-g729 -f asterisk-extra-sounds-en-g729-%{version}.list
%doc asterisk-extra-sounds-en-g729-%{version}.list

%files en-gsm -f asterisk-extra-sounds-en-gsm-%{version}.list
%doc asterisk-extra-sounds-en-gsm-%{version}.list

%files en-siren7 -f asterisk-extra-sounds-en-siren7-%{version}.list
%doc asterisk-extra-sounds-en-siren7-%{version}.list

%files en-siren14 -f asterisk-extra-sounds-en-siren14-%{version}.list
%doc asterisk-extra-sounds-en-siren14-%{version}.list

%files en-sln16 -f asterisk-extra-sounds-en-sln16-%{version}.list
%doc asterisk-extra-sounds-en-sln16-%{version}.list

%files en-ulaw -f asterisk-extra-sounds-en-ulaw-%{version}.list
%doc asterisk-extra-sounds-en-ulaw-%{version}.list

%files en-wav -f asterisk-extra-sounds-en-wav-%{version}.list
%doc asterisk-extra-sounds-en-wav-%{version}.list
%endif

%if %{with fr}
%files fr
%doc fr/extra-sounds-fr.txt
%doc fr/CHANGES-asterisk-extra-fr-%{version}
%doc fr/CREDITS-asterisk-extra-fr-%{version}
%license fr/LICENSE-asterisk-extra-fr-%{version}

%files fr-alaw -f asterisk-extra-sounds-fr-alaw-%{version}.list
%doc asterisk-extra-sounds-fr-alaw-%{version}.list

%files fr-g722 -f asterisk-extra-sounds-fr-g722-%{version}.list
%doc asterisk-extra-sounds-fr-g722-%{version}.list

%files fr-g729 -f asterisk-extra-sounds-fr-g729-%{version}.list
%doc asterisk-extra-sounds-fr-g729-%{version}.list

%files fr-gsm -f asterisk-extra-sounds-fr-gsm-%{version}.list
%doc asterisk-extra-sounds-fr-gsm-%{version}.list

%files fr-siren7 -f asterisk-extra-sounds-fr-siren7-%{version}.list
%doc asterisk-extra-sounds-fr-siren7-%{version}.list

%files fr-siren14 -f asterisk-extra-sounds-fr-siren14-%{version}.list
%doc asterisk-extra-sounds-fr-siren14-%{version}.list

%files fr-sln16 -f asterisk-extra-sounds-fr-sln16-%{version}.list
%doc asterisk-extra-sounds-fr-sln16-%{version}.list

%files fr-ulaw -f asterisk-extra-sounds-fr-ulaw-%{version}.list
%doc asterisk-extra-sounds-fr-ulaw-%{version}.list

%files fr-wav -f asterisk-extra-sounds-fr-wav-%{version}.list
%doc asterisk-extra-sounds-fr-wav-%{version}.list
%endif

%if %{with en_GB}
%files en_GB
%doc en_GB/extra-sounds-en_GB.txt
%doc en_GB/CHANGES-asterisk-extra-en_GB-%{version}
%doc en_GB/CREDITS-asterisk-extra-en_GB-%{version}
%license en_GB/LICENSE-asterisk-extra-en_GB-%{version}

%files en_GB-alaw -f asterisk-extra-sounds-en_GB-alaw-%{version}.list
%doc asterisk-extra-sounds-en_GB-alaw-%{version}.list

%files en_GB-g722 -f asterisk-extra-sounds-en_GB-g722-%{version}.list
%doc asterisk-extra-sounds-en_GB-g722-%{version}.list

%files en_GB-g729 -f asterisk-extra-sounds-en_GB-g729-%{version}.list
%doc asterisk-extra-sounds-en_GB-g729-%{version}.list

%files en_GB-gsm -f asterisk-extra-sounds-en_GB-gsm-%{version}.list
%doc asterisk-extra-sounds-en_GB-gsm-%{version}.list

%files en_GB-siren7 -f asterisk-extra-sounds-en_GB-siren7-%{version}.list
%doc asterisk-extra-sounds-en_GB-siren7-%{version}.list

%files en_GB-siren14 -f asterisk-extra-sounds-en_GB-siren14-%{version}.list
%doc asterisk-extra-sounds-en_GB-siren14-%{version}.list

%files en_GB-sln16 -f asterisk-extra-sounds-en_GB-sln16-%{version}.list
%doc asterisk-extra-sounds-en_GB-sln16-%{version}.list

%files en_GB-ulaw -f asterisk-extra-sounds-en_GB-ulaw-%{version}.list
%doc asterisk-extra-sounds-en_GB-ulaw-%{version}.list

%files en_GB-wav -f asterisk-extra-sounds-en_GB-wav-%{version}.list
%doc asterisk-extra-sounds-en_GB-wav-%{version}.list
%endif

%changelog
* Sun Jan 12 2020 Cloudonix Ops <ops@cloudonix.io> - 1.5.2-1
- Initial build for Cloudonix based on Fedora asteris-extra-sounds spec
