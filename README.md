# Asterisk Sounds Packaging

This repository contains build scripts to download and package the Asterisk Sounds distributables
for "Enterprise Linux" (RHEL, CentOS, and derivatives). These packages have a different release
cadence and versioning than the core Asterisk project, so they are maintained separately.

Packages are then deployed to the Cloudonix RPM repositories. See the [Cloudonix Asterisk Packages](https://gitlab.com/cloudonix-public/packages/asterisk)
repository for details on how to install and use.
